TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

HEADERS +=  headers/connection.h\
            headers/header.h\
            headers/mime_types.h\
            headers/reply.h\
            headers/request.h\
            headers/request_handler.h\
            headers/request_parser.h\
            headers/server.h\

SOURCES +=  src/main.cpp\
            src/connection.cpp\
            src/mime_types.cpp\
            src/reply.cpp\
            src/request_handler.cpp\
            src/request_parser.cpp\
            src/server.cpp\

INCLUDEPATH +=  headers\
                /usr/include/boost

LIBS += -L/usr/lib64/ -lboost_system\
        -L/usr/lib64/ -lboost_thread\
        -L/usr/lib64/ -lpthread
